

Curso: 
=====

Ejercicios HPC
==============


Ejemplos:
--------

- OpenMP
   * helloThread
   * pi

- MPI
   * helloTask
   * pi

- MPI + OpenMP
   * hello
   * pi
